What is API2Cart
================================
With API2Cart shopping platform integration is easy.

Overview
================================
API2Cart is a unified API that allows to integrate with 40+ shopping platforms and marketplaces like Magento, Shopify, WooCommerce, Amazon, eBay and others at once. 

Using API2Cart B2B software providers can easily connect to any supported eCommerce platform and never worry about developing separate connections. Moreover, API2Cart takes care of platform updates and maintains the integration.

How Does API2Cart Work
================================
API2Cart provides 100+ API methods to get, add, update, and sync various e-store data such as customers, orders, products, and categories, etc.

See all supported methods and platforms https://api2cart.com/supported-api-methods/.

To get started with API2Cart [register an account](https://app.api2cart.com/#register) for a 30-day free trial. Add stores and execute methods to see how you will be able to work with store entities.

Documentation For Authorization
================================
* [api_key](#markdown-header-span-elements)
      * [ Type: API key](#markdown-header-emphasis)
      * [API key parameter name: api_key](#markdown-header-emphasis)
      * [Location: URL query string](#markdown-header-emphasis)


* [store_key](#markdown-header-span-elements)
      * [Type: API key](#markdown-header-emphasis)
      * [API key parameter name: store_key](#markdown-header-emphasis)
      * [Location: URL query string](#markdown-header-emphasis)

Documentation for API Endpoints
================================
All URIs are relative to https://api.api2cart.com/v1.1

Our [documentation](https://docs.api2cart.com/) includes explanations, code samples and interactive examples. 

Support
================================
If you have any questions or issues, you can [contact us](https://api2cart.com/contact-us/) in whatever way is convenient for you. We provide full-tech 24/7 support.
